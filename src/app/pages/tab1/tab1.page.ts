import { Component } from '@angular/core';
import { DeseosService } from '../../services/deseos.service';
import { Lista } from '../../models/lista.model';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  listas: Lista [] = [];

  constructor( private deseosService: DeseosService,
               private router: Router,
               private alertControl: AlertController ) {
    this.listas = deseosService.listas;
  }

  async agregarLista(){
    // this.router.navigateByUrl('/tabs/tab1/agregar');

    const alert = await this.alertControl.create({
      // cssClass: 'my-custom-class',
      header: 'Nueva lista',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Nombre de la lista'
        }
      ],
      // buttons: ['OK']
      buttons: [
        {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar!!');
         }
        },
        {
          text: 'Crear',
          // role: 'cancel',
          handler: (data) => {
            // console.log(data);
            if (data.titulo.length === 0)  {
              return;
            } else {
              // crear lista
              const listaId = this.deseosService.crearLista(data.titulo);
              // console.log(listaId);
              this.router.navigateByUrl(`/tabs/tab1/agregar/${listaId }`);
            }
          }
        }
      ]
    });

    alert.present(); // await
  }

  // listaSeleccionada(lista: Lista) {
  //   // console.log(lista);
  //   // console.log(listaId);
  //   this.router.navigateByUrl(`/tabs/tab1/agregar/${lista.id }`);
  // }

}
