import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DeseosService } from '../../services/deseos.service';
import { Router } from '@angular/router';
import { Lista } from '../../models/lista.model';
import { AlertController, IonList } from '@ionic/angular';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  listas: Lista [] = [];
  @Input() terminado = true;
  @ViewChild( 'lista1' ) ionLista: IonList;

  constructor(private deseosService: DeseosService,
              private router: Router,
              private alertControl: AlertController) {
                this.listas = deseosService.listas;
              }

  ngOnInit() {}

  listaSeleccionada(lista: Lista) {
    // console.log(lista);
    // console.log(listaId);
    if (this.terminado) {
      this.router.navigateByUrl(`/tabs/tab2/agregar/${lista.id }`);
    } else {
      this.router.navigateByUrl(`/tabs/tab1/agregar/${lista.id }`);
    }
  }

  async editarLista(lista: Lista) {
    const alert = await this.alertControl.create({
      // cssClass: 'my-custom-class',
      header: 'Editar lista',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          value: lista.titulo,
          placeholder: 'Nombre de la lista'
        }
      ],
      // buttons: ['OK']
      buttons: [
        {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar!!');
          this.ionLista.closeSlidingItems();
         }
        },
        {
          text: 'Actualizar',
          // role: 'cancel',
          handler: (data) => {
            // console.log(data);
            if (data.titulo.length === 0)  {
              return;
            }

            // Actualizar lista
            lista.titulo = data.titulo;
            this.deseosService.guardarStorage();

            this.ionLista.closeSlidingItems();

          }
        }
      ]
    });

    alert.present();
  }

  borrarLista( lista: Lista) {
    this.deseosService.borrarLista(lista);
    this.listas = this.deseosService.listas;
  }

}
