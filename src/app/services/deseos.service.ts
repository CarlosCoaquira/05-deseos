import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas: Lista [] = [];

  constructor() {
    // console.log( ': Servicio Online');
    // const lista1 = new Lista('Item 1');
    // const lista2 = new Lista('Item 2');

    // this.listas.push( lista1, lista2);
    this.cargarStorage();
    // console.log(this.listas);
   }

   crearLista( titulo: string) {
    const nuevaLista = new Lista(titulo);
    this.listas.push( nuevaLista);
    this.guardarStorage();

    return nuevaLista.id;
   }

  //  actualizarLista( lista: Lista) {
  //   const indice = this.listas.filter( l => l.id === lista.id );
  //   this.listas.splice(indice, 1);
  //   this.guardarStorage();
  //  }

   borrarLista( lista: Lista) {
    this.listas = this.listas.filter ( l => l.id !== lista.id );

    this.guardarStorage();
   }

   obtenerLista( id: string | number) {
     id = Number(id);

     return this.listas.find(listaData =>  listaData.id === id );
   }

   guardarStorage() {
     // solo graba string
     localStorage.setItem('data', JSON.stringify (this.listas));
   }

   cargarStorage() {
     if (localStorage.getItem('data')) {
     this.listas = JSON.parse( localStorage.getItem('data')) ;
    } else {
      this.listas = [];
    }

   }
}
